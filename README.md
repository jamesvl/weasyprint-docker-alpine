Originally forked from [docker-weasyprint](https://github.com/aquavitae/docker-weasyprint)

Updated to use the latest version of Weasyprint from pip (currently at 59) and
Python 3.10.

Ubuntu 22.04 is the base OS.

# docker-weasyprint-alpine

[Weasyprint](http://weasyprint.org/) as a microservice in a podman image.

# Usage

Clone the repo, build the image, and run it, exposing port 5001.

```bash
$ git clone https://gitlab.com/jamesvl/weasyprint-docker-alpine.git
$ cd weasyprint-docker-alpine

$ podman build -t weasyprint .

# to run for testing, with console interaction, do
$ podman run --rm -t -i -p 127.0.0.1:5001:5001 --name weasyprint weasyprint

# to run in the background, do
$ podman run -d -p 127.0.0.1:5001:5001 --name weasyprint weasyprint
```

## Example

Send a `POST` to the path `/pdf` on port 5001, where the body of the POST
request is HTML. The server will send back a response whose body is a binary
PDF.

The filename may be set using a query parameter, e.g.:

```
curl -X POST -d @source.html http://127.0.0.1:5001/pdf?filename=result.pdf
```

This will use the file `source.html` and return a response with
`Content-Type: application/pdf` and `Content-Disposition: inline; filename=result.pdf`
headers.

See the [weasyprint docs](https://weasyprint.readthedocs.io/en/latest/features.html#)
for details about what CSS is valid and how images will be resolved.

## Health Check
In addition `/health` is a health check endpoint and a `GET` returns 'ok'.

## To Do

[x] publish to the public Docker repo so people can just pull down the image
[ ] optimize this image for size - I normally do a build layer and then a deploy
layer, but am having a hard time figuring out anything that can be left out
